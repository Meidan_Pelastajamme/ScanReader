package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/filesystem"
	"net/http"
)

func main() {
	err := gofiber_app()
	if err != nil {
		fmt.Println(err)
	}
}

func gofiber_app() error {
	app := fiber.New()

	app.Use(filesystem.New(filesystem.Config{
		Root:         http.Dir("./static"),
		Index:        "index.html",
		NotFoundFile: "error404.html",
	}))

	err := app.Listen(":8080")

	return err
}

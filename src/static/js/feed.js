function mobile_hover_toogle(this_element) {
    let the_parent = this_element.closest(".container"),
        children_list = the_parent.children

    let thumbnail       = children_list[0],
        title           = children_list[1],
        hover_details   = children_list[2],
        btn             = children_list[3]

    thumbnail.classList.toggle("mobile-thumbnail")
    title.classList.toggle("mobile-title")
    hover_details.classList.toggle("mobile-hover-details")
    btn.classList.toggle("mobile-hover-top-radius-btn")
}

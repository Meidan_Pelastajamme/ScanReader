function toggle_dropbtn_radius() {
    document.querySelector(".drop-btn").classList.toggle("clicked-drop-btn")
}

/* TOGGLE SHOW DROPDOWN AT USER CLICK ON DROPTBN */
function dropdown_handler() {
    document.getElementById("dropdown").classList.toggle("show")
    toggle_dropbtn_radius()
}

// REMOVE "show" CLASS FROM "dropdown-content" IF USER CLICK ANYWHERE ELSE ON THE SCREEN
window.onclick = function (event) {
    if (event.target.matches('.drop-btn')) return

    let dropdowns = document.getElementsByClassName("dropdown-content")
    for (let i = 0; i < dropdowns.length; i++) {
        let openDropdown = dropdowns[i]
        if (!openDropdown.classList.contains('show')) return
        openDropdown.classList.remove('show')
        toggle_dropbtn_radius()
    }
}
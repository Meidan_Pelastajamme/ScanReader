// const   button_increment = document.querySelector(".decrement"),
//         button_decrement = document.querySelector(".increment"),

//         button_goto_end = document.querySelector(".goto-end"),
//         button_goto_start = document.querySelector(".goto-start"),

const   displayed_card = document.querySelector(".displayed-card"),

        displayed_card_number = document.querySelector(".displayed-card-number"),
        dropdown_btn = document.querySelector(".drop-btn"),

        max_card = 3
let     count = 1,
        just_initialized = true

function initialize(count) {
    // initialize counter
    dropdown_btn.innerHTML = count + " | " + max_card;
    // Highlight the first dropdown's "a" 
    document.querySelector(".dropdown-content").children[--count].classList.add("highlight-dropdowns-a")
}; initialize(count)

// Keyboard control
document.onkeydown = function (e) {
    switch (e.key) {
        case 'ArrowLeft':
        case 'a':
        case 'q':
            decrement(); break

        case 'ArrowRight':
        case 'd':
        case ' ':
            increment(); break
    }
}

function update_dropdown_highlight(count, just_initialized) {
    let dropdown_content_div = document.querySelector(".dropdown-content"),
        pages_link_list = dropdown_content_div.children
    if(just_initialized != true) document.querySelector('.highlight-dropdowns-a').classList.remove("highlight-dropdowns-a")
    pages_link_list[--count].classList.add("highlight-dropdowns-a") // "--count" to make le link between a list of [1,2,3] and list positions of [0,1,2]
    return ++just_initialized
}

function goto_n_page(element) {
    let page_str = element.firstChild.data
    page_str = page_str.replace("Page ", "")
    let page_int = Number(page_str)

    if (page_int === count || page_int > max_card) return

    if (page_int > count) {
        update_previous_card(count)
        count = page_int
        update_next_card(count)
        update_counter(count)
        return count
    } else {
        update_previous_card(count)
        count = page_int
        update_next_card(count)
        update_counter(count)
        return count
    }
}

function update_counter(count) {
    dropdown_btn.innerHTML = count + " | " + max_card
    update_dropdown_highlight(count)
}

function update_previous_card(count) {
    let previous_card_class = ".card" + count
    const previous_card = document.querySelector(previous_card_class)
    previous_card.classList.remove('displayed-card')
}
function update_next_card(count) {
    let next_card_class = ".card" + count
    const next_card = document.querySelector(next_card_class)
    next_card.classList.add('displayed-card')
}

function increment() {
    if (count >= max_card) return

    update_previous_card(count)
    count++
    update_next_card(count)
    update_counter(count)

    return count
}

function decrement() {
    if (count == 1) return

    update_previous_card(count)
    count--
    update_next_card(count)
    update_counter(count)

    return count
}

function goto_end() {
    if (count >= max_card) return

    update_previous_card(count)
    count = max_card
    update_next_card(count)
    update_counter(count)

    return count
}

function goto_start() {
    if (count == 1) return

    update_previous_card(count)
    count = 1
    update_next_card(count)
    update_counter(count)

    return count
}


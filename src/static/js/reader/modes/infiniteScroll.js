const speed1 = 70,
    speed2 = 120,
    speed3 = 140,
    speed4 = 180,
    speed5 = 250
const maxUserSpeed = 5
let currentUserSpeed = 1,
    speed,      // undefined
    isSpeedCHanged,    // undefined
    scrolling = false

// ONCLICK LISTENERS
const playButton = document.querySelector(".play-button"),
    speedButton = document.querySelector(".speed-button")

playButton.onclick = function (event) {
    if (scrolling == false) {
        checkAndRun()
    } else {
        scrolling = false
        return scrolling
    }
}

speedButton.onclick = function (event) {
    if (currentUserSpeed >= maxUserSpeed) {
        speedButton.innerHTML = 1
        currentUserSpeed = 1
    } else {
        currentUserSpeed++
        speedButton.innerHTML = currentUserSpeed
    }
    if (scrolling == true) {
        isSpeedCHanged = true
        checkAndRun()
    }
}
//
// Convert the user speed (here: 1 - 3) to js scrollBy speed (here: 100 - 300)
function speedChecker() {
    switch (currentUserSpeed) {
        case 1: return speed1
        case 2: return speed2
        case 3: return speed3
        case 4: return speed4
        case 5: return speed5
    }
}
//
// check speed changes and run the infiniteScrolldown function
function checkAndRun() {
    speed = speedChecker()
    scrolling = true
    return scrolling, infiniteScrolldown(speed)
}

// Scroll infinitly the window 
// until :
// - the end of the window,
// - speed changes
// - user click on any other page elements 
function infiniteScrolldown(speed) {
    let totalScroll = document.body.scrollHeight,   // Scroll height of the page
        duration = totalScroll / speed * 1000,      // Duration of scroll in milliseconds
        steps = Math.ceil(duration / 1000 * 60),    // Number of steps needed
        scrollPerStep = totalScroll / steps         // Distance to scroll per steps

    // Recursive function that call itself at each steps
    function step() {
        window.scrollBy(0, scrollPerStep)
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            scrolling = false
            return
        }
        if (scrolling != true) return
        if (isSpeedCHanged == true) return isSpeedCHanged = false

        window.requestAnimationFrame(step)
    }
    window.requestAnimationFrame(step)
}

// IMAGE BY IMAGE SCROLL

// Keyboard control
document.onkeydown = function (e) {
    switch (e.key) {
        case ' ': nextImage(); e.preventDefault(); break
    }
}

let currentImage = 0
let images = document.getElementsByTagName("img")
let numberOfImages = images.length
let image;
let firstImageOffsetTop = images[0].offsetTop
function nextImage() {
    image = images[currentImage]
    console.log(image)
    console.log(images[0].offsetTop)
    if (document.scrollTop <= firstImageOffsetTop) {
        images[0].scrollIntoView()
        currentImage = 0
        return
    } 
    if (currentImage == numberOfImages) return
    image.scrollIntoView()
    currentImage++
}
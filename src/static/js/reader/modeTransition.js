// REMOVE JS FROM CURRENT MODE & CLEAR ALL CSS RELATED
// ADD JS FROM NEXT MODE (THAT BECAME THE CURRENT MODE) & AND ADD ALL CSS RELATED

function remove_previous_mode_css() {
    let hrefElemToDelete = "css/reader/reader.css"
    let elementToDelete = document.querySelector(`link[href='${hrefElemToDelete}']`)
    elementToDelete.parentNode.removeChild(elementToDelete)
}

function add_next_mode_css(linkHref) {
    const link = document.createElement('link')
    link.rel = 'stylesheet'
    link.href = linkHref
    document.getElementsByTagName('head')[0].appendChild(link)
}

let linkHref = 'css/reader/modes/test.css'
// add_next_mode_css(linkHref)


# Scan Reader

/!\ *REMEMBER: When the project is advanced enough, add a dumb, ultra permissive license* /!\

### Feature :
- [ ] Good enough UI/UX
- [ ] Dynamic content (with DB) 
- [ ] Add more complex dynamix content with category, tags etc
- [ ] Integrate LiveSearch to it 
- [ ] Make a more extensive README with : Installation (Project, DB, Data formatting), Usage,  
